<?php
/**
 * AppDetails.php
 */

/**
 * AppDetails
 *
 * @author Csaba Keri <kericsaba89@gmail.com>
 * @version 1.0.0
 */
class AppDetails {

    const CACHE_FILENAME_PREFIX = 'AppDetails_';

    /**
     * The parameters for 42matters' lookup API.
     *
     * (string) url: The 42matters API's url. (required)
     * (string) access_token: Your access token for using 42matters' API. (required)
     * (string) p: The package name identifying an Android application. (required)
     * (string) lang: The language you want to use when returning app data.
     * (string) fields: Comma-separated list of fields to return.
     *
     * @var array $requestSettings
     */
    protected $requestSettings = array(
        'url' => 'https://42matters.com/api/1/apps/lookup.json',
        'access_token' => null,
        'p' => null,
        'lang' => null,
        'fields' => null
    );

    /**
     * The cache settings.
     *
     * It's store the 42matters API's response in a JSON file.
     *
     * (bool) enabled: If true, the 42matters API's response will be cached. Default: false
     * (string) file_path: The path where the file will be saved. Default: __DIR__
     * (int) expiration: The cached file expiration time in minutes. Default: 60, if 0 then cached while the stored JSON file is available
     *
     * @var array $cacheSettings
     */
    protected $cacheSettings = array(
        'enabled' => false,
        'file_path' => '',
        'expiration' => 60
    );

    /**
     * The cached file's name.
     *
     * Format: [CACHE_FILENAME_PREFIX]_[package]_[lang].json
     * Example: AppDetails_com.darkcherrysoft.games.Hitit_en.json
     *
     * @var string
     */
    protected $cacheFilename = '';

    /** @var string The pure JSON response. */
    protected $response;
    /** @var array The response in array format. */
    protected $responseArray;

    /**
     * The variables represents the 42matters API's response schema.
     */
    protected $packageName;
    protected $category;
    protected $catInt;
    protected $catKey;
    protected $catType;
    protected $contentRating;
    protected $description;
    protected $downloads;
    protected $created;
    protected $marketUpdate;
    protected $promoVideo;
    protected $rating;
    protected $size;
    protected $screenshots;
    protected $title;
    protected $version;
    protected $website;
    protected $developer;
    protected $numberRatings;
    protected $priceCurrency;
    protected $priceNumeric;
    protected $price;
    protected $iap;
//    protected $iapMin;
//    protected $iapMax;
    protected $i18nLang;
    protected $icon;
    protected $icon72;
    protected $deepLink;
    protected $marketUrl;

    /**
     * Perform request, cache settings and call the 42matters' API.
     *
     * @param array $requestSettings Info: $this->requestSettings.
     * @param array|null $cacheSettings Info: $this->cacheSettings.
     */
    public function __construct(array $requestSettings, array $cacheSettings = null) {
        $this->setRequestSettings($requestSettings);
        $this->setCacheSettings($cacheSettings);
        $this->request();
    }

    /**
     * @return array
     */
    public function getRequestSettings() {
        return $this->requestSettings;
    }

    /**
     * @return array
     */
    public function getCacheSettings() {
        return $this->cacheSettings;
    }

    /**
     * @return string
     */
    public function getCachedFilename() {
        return $this->cacheFilename;
    }

    /**
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * @return array
     */
    public function getResponseArray() {
        return $this->responseArray;
    }

    /**
     * @return string
     */
    public function getPackageName() {
        return $this->packageName;
    }

    /**
     * @return string
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @return int
     */
    public function getCatInt() {
        return (int)$this->catInt;
    }

    /**
     * @return string
     */
    public function getCatKey() {
        return $this->catKey;
    }

    /**
     * @return int
     */
    public function getCatType() {
        return (int)$this->catType;
    }

    /**
     * @return string
     */
    public function getContentRating() {
        return $this->contentRating;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getDownloads() {
        return $this->downloads;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreated() {
        if(!empty($this->created)) {
            return new \DateTime($this->created);
        }
        return null;
    }

    /**
     * @return \DateTime|null
     */
    public function getMarketUpdate() {
        if(!empty($this->marketUpdate)) {
            return new \DateTime($this->marketUpdate);
        }
        return null;
    }

    /**
     * @return string
     */
    public function getPromoVideo() {
        return $this->promoVideo;
    }

    /**
     * @return double
     */
    public function getRating() {
        return (double)$this->rating;
    }

    /**
     * @return int
     */
    public function getSize() {
        return (int)$this->size;
    }

    /**
     * @return array
     */
    public function getScreenshots() {
        return $this->screenshots;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * @return string
     */
    public function getDeveloper() {
        return $this->developer;
    }

    /**
     * @return int
     */
    public function getNumberRatings() {
        return (int)$this->numberRatings;
    }

    /**
     * @return string
     */
    public function getPriceCurrency() {
        return $this->priceCurrency;
    }

    /**
     * @return double
     */
    public function getPriceNumeric() {
        return (double)$this->priceNumeric;
    }

    /**
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function getIap() {
        return $this->iap;
    }

    /**
     * @return double
     */
//    public function getIapMin() {
//        return (double)$this->iapMin;
//    }

    /**
     * @return double
     */
//    public function getIapMax() {
//        return (double)$this->iapMax;
//    }

    /**
     * @return array
     */
    public function getI18nLang() {
        return $this->i18nLang;
    }

    /**
     * @return string
     */
    public function getIcon() {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getIcon72() {
        return $this->icon72;
    }

    /**
     * @return string
     */
    public function getDeepLink() {
        return $this->deepLink;
    }

    /**
     * @return string
     */
    public function getMarketUrl() {
        return $this->marketUrl;
    }

    /**
     * Send request and handle the response, or load response from cache.
     *
     * @throws Exception
     */
    public function request() {
        if($this->checkCache()) {
            $this->response = file_get_contents($this->cacheSettings['file_path'] . $this->cacheFilename);
            $this->responseArray = json_decode($this->response, true);
            $this->fillResponseProperties();
        } else {
            $request = curl_init();
            curl_setopt_array($request, array(
                CURLOPT_URL => $this->buildUrl(),
                CURLOPT_BINARYTRANSFER => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => true
            ));
            $this->response = curl_exec($request);
            if(curl_getinfo($request, CURLINFO_HTTP_CODE) === 200) {
                $this->responseArray = json_decode($this->response, true);
                $this->fillResponseProperties();
                $this->caching();
            } else {
                throw new \Exception('Bad request! HTTP code: ' . curl_getinfo($request, CURLINFO_HTTP_CODE));
            }
            curl_close($request);
        }
    }

    /**
     * Remove the cached JSON file.
     *
     * @param string|null $filename The cached file's full name. If null, removes all cached files.
     */
    public function clearCache($filename = null) {
        if(!is_null($filename)) {
            $file = $this->cacheSettings['file_path'] . $filename;
            if(file_exists($file)) {
                unlink($file);
            }
        } else {
            $files = array();
            self::searchFilesInPath($this->cacheSettings['file_path'], $files, array('json'), false);
            if(!empty($files)) {
                foreach($files as $file) {
                    if(strpos($file, self::CACHE_FILENAME_PREFIX) !== false) {
                        unlink($file);
                    }
                }
            }
        }
    }

    /**
     * Set the $this->requestSettings array.
     *
     * @param array $requestSettings Info: $this->requestSettings.
     * @throws Exception
     */
    private function setRequestSettings(array $requestSettings) {
        if(!empty($requestSettings)) {
            if(array_key_exists('access_token', $requestSettings)) {
                if(array_key_exists('p', $requestSettings)) {
                    if(array_key_exists('url', $requestSettings)) {
                        if(empty($requestSettings['url'])) throw new \Exception('The url is required!');
                    }
                    foreach($requestSettings as $key => $value) {
                        if(array_key_exists($key, $this->requestSettings)) {
                            $this->requestSettings[$key] = $value;
                        }
                    }
                } else {
                    throw new \Exception('The app\'s packege name is required!');
                }
            } else {
                throw new \Exception('The access token is required!');
            }
        } else {
            throw new \Exception('The request settings are required!');
        }
    }

    /**
     * Set the $this->cacheSettings array.
     *
     * @param array $cacheSettings Info: $this->cacheSettings.
     * @throws Exception
     */
    private function setCacheSettings(array $cacheSettings = null) {
        $this->cacheFilename = self::CACHE_FILENAME_PREFIX . $this->requestSettings['p'] . '_'
            . (!empty($this->requestSettings['lang']) ? $this->requestSettings['lang'] : 'en')
            . '.json';
        if(!empty($cacheSettings) && !is_null($cacheSettings)) {
            if(array_key_exists('enabled', $cacheSettings) && $cacheSettings['enabled'] === true) {
                $this->cacheSettings['enabled'] = true;
                // check file path
                if((array_key_exists('file_path', $cacheSettings))) {
                    if(is_dir($cacheSettings['file_path'])) {
                        if(is_writable($cacheSettings['file_path'])) {
                            $this->cacheSettings['file_path'] = $cacheSettings['file_path'];
                            if(substr($this->cacheSettings['file_path'], -1) != '/') {
                                $this->cacheSettings['file_path'] .= '/';
                            }
                        } else {
                            throw new \Exception('The cache\'s file path must be writable!');
                        }
                    } else {
                        throw new \Exception('The cache\'s file path must be a directory!');
                    }
                } else {
                    if(is_writable(__DIR__ . '/')) {
                        $this->cacheSettings['file_path'] = __DIR__ . '/';
                    } else {
                        throw new \Exception('The cache\'s file path must be writable!');
                    }
                }
                // check expiration time
                if((array_key_exists('expiration', $cacheSettings))) {
                    if(is_numeric($cacheSettings['expiration'])) {
                        $this->cacheSettings['expiration'] = abs((int)$cacheSettings['expiration']);
                    } else {
                        throw new \Exception('The cached file\'s expiration time must be numeric!');
                    }
                }
            }
        }
    }

    /**
     * Build the request url from settings array.
     *
     * @return string
     */
    private function buildUrl() {
        $url = $this->requestSettings['url'] . '?';
        $url .= implode('&', array_filter(
            array_map(function($key, $val) {
                if(!is_null($val) && $key != 'url') return urlencode($key) . '=' . urlencode($val);
                else return null;
            }, array_keys($this->requestSettings), $this->requestSettings)
        ));
        return $url;
    }

    /**
     * Convert underline_words to camelCase format.
     *
     * @param string $string The string you need to format.
     * @return string
     */
    private function formatUnderlineStringToCamelCase($string) {
        if (strpos($string, '_') !== false) {
            $words = explode('_', $string);
            return lcfirst(implode('', array_map(function ($word) {
                return ucfirst($word);
            }, $words)));
        } else {
            return $string;
        }
    }

    /**
     * Fill member variables with response data.
     */
    private function fillResponseProperties() {
        if (!empty($this->responseArray) && is_array($this->responseArray)) {
            foreach ($this->responseArray as $key => $value) {
                $key = $this->formatUnderlineStringToCamelCase($key);
                if (property_exists($this, $key)) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * Store response in a JSON file.
     */
    private function caching() {
        if($this->cacheSettings['enabled'] && !empty($this->response)) {
            $fileResource = fopen($this->cacheSettings['file_path'] . $this->cacheFilename, 'w');
            fwrite($fileResource, $this->response);
        }
    }

    /**
     * Return true if chache enabled, cache file exists, and expiration time is 0 or has not expired.
     *
     * @return bool
     */
    private function checkCache() {
        $file = $this->cacheSettings['file_path'] . $this->cacheFilename;
        if($this->cacheSettings['enabled'] && file_exists($file)) {
            if($this->cacheSettings['expiration'] == 0) {
                return true;
            } else {
                $deadline = new \DateTime();
                $deadline->sub(new DateInterval('PT' . $this->cacheSettings['expiration'] . 'M'));
                $fileModifyDate = new \DateTime(date('Y-m-d H:i', filemtime($file)));
                if($deadline <= $fileModifyDate) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Searching for files in the specified folder and its subfolders recursively.
     *
     * @param string $path The folder's path
     * @param array $files The result array
     * @param array $extensions The extensions, which are examined
     * @param bool $subFolders If true traverses subfolders.
     */
    protected static function searchFilesInPath($path, array &$files, array $extensions = array(), $subFolders = true) {
        $scandir = scandir($path);
        if(!empty($scandir)) {
            foreach ($scandir as $file) {
                if($file[0] != '.') {
                    if(is_dir($path . $file)) {
                        if($subFolders) {
                            self::searchFilesInPath($path . $file . '/', $files, $extensions);
                        }
                    } else if(is_file($path . $file)) {
                        if(!empty($extensions)) {
                            $fileExtension = pathinfo($path . $file, PATHINFO_EXTENSION);
                            if(in_array($fileExtension, $extensions)) {
                                array_push($files, $path . $file);
                            }
                        } else {
                            array_push($files, $path . $file);
                        }
                    }
                }
            }
        }
    }
}
