<?php
/**
 * AppDetailsImages.php
 */

require_once('AppDetails.php');

/**
 * AppDetailsImages
 *
 * @author Csaba Keri <kericsaba89@gmail.com>
 * @version 1.0.0
 */
class AppDetailsImages extends AppDetails {
    protected $imageDir;
    protected $screenshotsPaths = array();
    protected $screenshotsFiles = array();
    protected $iconPath;
    protected $iconFile;
    protected $icon72Path;
    protected $icon72File;

    /**
     * Call parent, set image base dir and collect images of the application.
     *
     * @param string $path The folder where the images are stored.
     * @param array $requestSettings Info: $this->requestSettings.
     * @param array|null $cacheSettings Info: $this->cacheSettings.
     * @param bool $fetchImages If true, calls fetchImages method automatically.
     * @throws Exception
     */
    public function __construct($path, array $requestSettings, array $cacheSettings = null, $fetchImages = true) {
        parent::__construct($requestSettings, $cacheSettings);
        if(is_dir($path)) {
            $this->imageDir = $path;
            if(substr($this->imageDir, -1) != '/') {
                $this->imageDir .= '/';
            }
            if($fetchImages) {
                $this->fetchImages();
            }
        } else {
            throw new \Exception('The path is not a directory!');
        }
    }

    /**
     * @return array
     */
    public function getScreenshotsPaths() {
        return $this->screenshotsPaths;
    }

    /**
     * @return array
     */
    public function getScreenshotsFiles() {
        return $this->screenshotsFiles;
    }

    /**
     * @return string
     */
    public function getIconPath() {
        return $this->iconPath;
    }

    /**
     * @return string
     */
    public function getIconFile() {
        return $this->iconFile;
    }

    /**
     * @return string
     */
    public function getIcon72Path() {
        return $this->icon72Path;
    }

    /**
     * @return string
     */
    public function getIcon72File() {
        return $this->icon72File;
    }

    /**
     * Collect images of the application.
     */
    public function fetchImages() {
        // screenshots
        if(is_array($this->screenshots) && !empty($this->screenshots)) {
            $i = 1;
            foreach($this->screenshots as $url) {
                $dir = $this->imageDir . $this->requestSettings['p'] . '/';
                $file = 'screenshot' . $i . '.png';
                if(file_exists($dir . $file)) {
                    $this->screenshotsPaths[] = $dir . $file;
                    $this->screenshotsFiles[] = $file;
                } else {
                    $this->screenshotsPaths[] = self::saveImageFromUrl($url, $dir, $file);
                    $this->screenshotsFiles[] = $file;
                }
                $i++;
            }
        }
        // icon
        if(!empty($this->icon)) {
            $dir = $this->imageDir . $this->requestSettings['p'] . '/';
            $file = 'icon.png';
            if(file_exists($dir . $file)) {
                $this->iconPath = $dir . $file;
                $this->iconFile = $file;
            } else {
                $this->iconPath = self::saveImageFromUrl($this->icon, $dir, $file);
                $this->iconFile = $file;
            }
        }
        // icon 72
        if(!empty($this->icon72)) {
            $dir = $this->imageDir . $this->requestSettings['p'] . '/';
            $file = 'icon72.png';
            if(file_exists($dir . $file)) {
                $this->icon72Path = $dir . $file;
                $this->icon72File = $file;
            } else {
                $this->icon72Path = self::saveImageFromUrl($this->icon72, $dir, $file);
                $this->icon72File = $file;
            }
        }
    }

    /**
     * Remove the app's stored images.
     *
     * @param string|null $filename The stored image file's name (relative to [image_dir]/[package_name]/).
     * If null, removes all stored images and the image folder of the app.
     *
     * @throws Exception
     */
    public function removeImages($filename = null) {
        $dir = $this->imageDir . $this->requestSettings['p'] . '/';
        if(is_dir($dir)) {
            if(!is_null($filename)) {
                $file = $dir . $filename;
                if(file_exists($file)) {
                    unlink($file);
                }
            } else {
                $files = array();
                self::searchFilesInPath($dir, $files, array('png'), false);
                if(!empty($files)) {
                    foreach($files as $file) {
                        unlink($file);
                    }
                    rmdir($dir);
                }
            }
        } else {
            throw new \Exception('The following directory isn\'t exists: ' . $dir);
        }
    }

    /**
     * Download image from specific url.
     *
     * @param string $fromUrl The image url.
     * @param string $toDir The 'save' directory
     * @param string $toFileName The 'save' filename
     * @return string Path of the saved file.
     * @throws Exception
     */
    private static function saveImageFromUrl($fromUrl, $toDir, $toFileName) {
        if(!is_dir($toDir)) {
            mkdir($toDir);
        }
        $file = $toDir . $toFileName;
        $ch = curl_init($fromUrl);
        $fp = fopen($toDir . $toFileName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        fclose($fp);
        if($httpCode === 200) {
            return $file;
        } else {
            throw new \Exception('Bad request! HTTP code: ' . $httpCode);
        }
    }
}
