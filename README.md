[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a195c314-e191-4342-b0ab-e4859fe990d7/big.png)](https://insight.sensiolabs.com/projects/a195c314-e191-4342-b0ab-e4859fe990d7)

# AppDetails #

PHP class to query data from a specific app for android with the 42matters' lookup API.

## Usage ##

To be able to use this you will require an access token from [42matters' website](https://42matters.com/).

## Getting Started ##

Just create an instance of AppDetails class:

```
#!php
$appDetails = new AppDetails(array(
   // Request settings here
), array(
   // Cache settings here
));
```


For example get the application called "Pandapuff" data:

```
#!php
$appDetails = new AppDetails(array(
    'access_token' => 'YOUR ACCESS TOKEN',
    'p' => 'com.darkcherrysoft.games.Hitit'
));
// $appDetails store the returned app data
echo $appDetails->getTitle(); // It prints the app's name 'Pandapuff'
```

More examples can be found in the 'example' folder.

## Request settings ##

* **url** - (*string*) The 42matters API's url. (required) - **default**: https://42matters.com/api/1/apps/lookup.json (should not be modified)
* **access_token** - (*string*) An access token from 42matters' website. (required)
* **p** - (*string*) The package name identifying an Android application. (required)
* **lang** - (*string*) The language you want to use when returning app data. - **default**: en
* **fields** - (*string*) Comma-separated list of fields to return. Available fields [here](https://42matters.com/api/lookup). - **default**: all fields returned

## Cache settings ##

AppDetails can store the 42matters API's response in a JSON file.

* **enabled** - (*bool*) If true, the 42matters API's response will be cached. **Default**: false
* **file_path** - (*string*) The path where the file will be saved. **Default**: The directory of AppDetails.php.
* **expiration** - (*string*) The cached file expiration time in minutes. **Default**: 60, if 0 then cached while the stored JSON file is available
