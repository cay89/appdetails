<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

require_once(__DIR__ . '/../AppDetails.php');
require_once(__DIR__ . '/../AppDetailsImages.php');

/**
 * EXAMPLE
 *
 * Uncomment the section what you want to try.
 */

try {
    // Basic example BEGIN *********************************************************************************************

//    $appDetails = new AppDetails(array(
//        'access_token' => 'YOUR ACCESS TOKEN',
//        'p' => 'com.darkcherrysoft.games.Hitit'
//    ));
//    echo $appDetails->getTitle();

    // Basic example END ***********************************************************************************************

    // Caching BEGIN ***************************************************************************************************

//    $appDetails = new AppDetails(array(
//        'access_token' => 'YOUR ACCESS TOKEN',
//        'p' => 'com.darkcherrysoft.games.Hitit'
//    ), array(
//        'enabled' => true,
//        'expiration' => 1
//    ));
//    $cacheSettings = $appDetails->getCacheSettings();
//    echo $appDetails->getTitle() . ' data is cached here: ' . $cacheSettings['file_path'] . $appDetails->getCachedFilename();

    // Caching END *****************************************************************************************************

    // Caching more then one request BEGIN *****************************************************************************

//    $appDetails1 = new AppDetails(array(
//        'access_token' => 'YOUR ACCESS TOKEN',
//        'p' => 'com.google.android.music'
//    ), array(
//        'enabled' => true,
//        'expiration' => 1
//    ));
//    $appDetails2 = new AppDetails(array(
//        'access_token' => 'YOUR ACCESS TOKEN',
//        'p' => 'com.google.android.music',
//        'lang' => 'de'
//    ), array(
//        'enabled' => true,
//        'expiration' => 1
//    ));
//    $cacheSettings1 = $appDetails1->getCacheSettings();
//    echo $appDetails1->getTitle() . ' english data is cached here: ' . $cacheSettings1['file_path'] . $appDetails1->getCachedFilename();
//    $cacheSettings2 = $appDetails2->getCacheSettings();
//    echo ' and the german is here: ' . $cacheSettings2['file_path'] . $appDetails2->getCachedFilename();

    // Caching more then one request END *******************************************************************************

    // Download images BEGIN *******************************************************************************************

//    $appDetailsImages = new AppDetailsImages(__DIR__, array(
//        'access_token' => 'YOUR ACCESS TOKEN',
//        'p' => 'com.darkcherrysoft.games.Hitit'
//    ), array(
//        'enabled' => true,
//        'expiration' => 0
//    ));

    // Download images END *********************************************************************************************
} catch (\Exception $ex) {
    echo $ex->getMessage();
}
